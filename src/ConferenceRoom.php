<?php
namespace LibreOffice;

use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Assets\Image;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DataObject;
use SilverStripe\Versioned\Versioned;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;


class ConferenceRoom extends DataObject
{
    private static $table_name = 'LibreOfficeConferenceRoom';

    private static $db = [
        'Name' => 'Varchar',
    ];

    private static $has_one = [
        'ConferencePage' => ConferencePage::class,
    ];

    private static $has_many = [
        'Events' => ConferenceEvent::class,
    ];

    private static $owns = [
        'Events',
    ];

    private static $extensions = [
        Versioned::class,
    ];

    private static $summary_fields = [
        'Name',
    ];

    public function getCMSFields() {
        $fields = FieldList::create(
            TextField::create('Name'),
            GridField::create(
                'Events',
                'Events on this room',
                $this->Events(),
                GridFieldConfig_RecordEditor::create()
            )
        );

        return $fields;
    }
}

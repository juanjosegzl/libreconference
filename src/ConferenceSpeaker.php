<?php
namespace LibreOffice;

use SilverStripe\ORM\DataObject;
use SilverStripe\Assets\Image;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Versioned\Versioned;

class ConferenceSpeaker extends DataObject
{
    private static $table_name = 'LibreOfficeConferenceSpeaker';
    
    private static $db = [
        'FullName' => 'Varchar',
        'Title' => 'Varchar',
    ];

    private static $has_one = [
        'Picture' => Image::class,
        'ConferencePage' => ConferencePage::class,
    ];

    private static $has_many = [
        'Events' => ConferenceEvent::class,
    ];

    private static $owns = [
        'Picture',
    ];

    private static $extensions = [
        Versioned::class
    ];

    private static $summary_fields = [
        'FullName' => 'Full name',
        'Title'
    ];

    public function getCMSFields() {
        $fields = FieldList::create(
            TextField::create('FullName'),
            TextField::create('Title'),
            $uploader = UploadField::Create('Picture')
        );

        $uploader->setFolderName('conference-speakers-pictures');
        $uploader->getValidator()->setAllowedExtensions(['png', 'gif', 'jpg', 'jpeg']);

        return $fields;
    }
}

<?php
namespace LibreOffice;

use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DataObject;
use SilverStripe\Versioned\Versioned;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;

class ConferenceSection extends DataObject
{
    private static $table_name = 'LibreOfficeConferenceSection';

    private static $db = [
        'Title' => 'Varchar',
        'Content' => 'HTMLText',
    ];

    private static $has_one = [
        'ConferencePage' => ConferencePage::class,
    ];

    private static $extensions = [
        Versioned::class,
    ];

    private static $summary_fields = [
        'Title',
    ];

    public function getCMSFields() {
        $fields = FieldList::create(
            TextField::create('Title'),
            HTMLEditorField::create('Content')
        );

        return $fields;
    }
}

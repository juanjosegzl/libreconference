<?php
namespace LibreOffice;

use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Assets\Image;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\DatetimeField;
use SilverStripe\Forms\DropdownField;
use SilverStripe\ORM\DataObject;
use SilverStripe\Versioned\Versioned;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;

class ConferenceEvent extends DataObject
{
    private static $table_name = 'LibreOfficeConferenceEvent';

    private static $db = [
        'Title' => 'Varchar',
        'Description' => 'HTMLText',
        'Starting' => 'Datetime',
    ];

    private static $has_one = [
        'Speaker' => ConferenceSpeaker::class,
        'Room' => ConferenceRoom::class,
    ];

    private static $extensions = [
        Versioned::class,
    ];

    private static $summary_fields = [
        'Title',
        'Description',
        'Starting',
    ];

    public function getCMSFields() {
        $fields = FieldList::create(
            TextField::create('Title'),
            HTMLEditorField::create('Description'),
            DatetimeField::create('Starting'),
            DropdownField::create(
                'SpeakerID',
                'Speaker',
                ConferenceSpeaker::get()->map('ID', 'FullName')
            )->setEmptyString('(Select one)')
        );

        return $fields;
    }
}

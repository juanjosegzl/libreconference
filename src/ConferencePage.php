<?php

namespace LibreOffice;

use Page;
use SilverStripe\Assets\Image;
use SilverStripe\Forms\DatetimeField;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;

class ConferencePage extends Page
{
    private static $table_name = 'LibreOfficeConferencePage';

    private static $db = [
        'Title' => 'Varchar',
        'Inauguration' => 'Datetime',
        'About' => 'HTMLText'
    ];

    private static $has_one = [
        'Logo' => Image::class,
        'HeroBanner' => Image::class,
    ];

    private static $has_many = [
        'Sections' => ConferenceSection::class,
        'Speakers' => ConferenceSpeaker::class,
        'Sponsors' => ConferenceSponsor::class,
        'Locations' => ConferenceLocation::class,
        'Rooms' => ConferenceRoom::class,
    ];

    private static $owns = [
        'Sections',
        'Logo',
        'HeroBanner',
        'Speakers',
        'Sponsors',
        'Locations',
        'Rooms',
    ];

    public function getCMSFields() {
        $fields = parent::getCMSFields();

        $fields->removeFieldFromTab('Root.Main', 'Content');

        $fields->addFieldToTab('Root.Conference', TextField::create('Title'));
        $fields->addFieldToTab('Root.Conference', DatetimeField::create('Inauguration','Date of inauguration'));   
        $fields->addFieldToTab('Root.Conference', HTMLEditorField::create('About'));

        $fields->addFieldToTab('Root.Conference', GridField::create(
            'Sections',
            'Sections on this page',
            $this->Sections(),
            GridFieldConfig_RecordEditor::create()
        ));

        $logoUploader = UploadField::create('Logo');
        $logoUploader->setFolderName('conference-logos');
        $logoUploader->getValidator()->setAllowedExtensions(['png', 'gif', 'jpg', 'jpeg']);

        $fields->addFieldToTab('Root.Conference', $logoUploader);

        $uploader = UploadField::create('HeroBanner');
        $uploader->setFolderName('conference-hero-banners');
        $uploader->getValidator()->setAllowedExtensions(['png', 'gif', 'jpg', 'jpeg']);

        $fields->addFieldToTab('Root.Conference', $uploader);

        $fields->addFieldToTab('Root.Speakers', GridField::create(
            'Speakers',
            'Speakers on this conference',
            $this->Speakers(),
            GridFieldConfig_RecordEditor::create()
        ));

        $fields->addFieldToTab('Root.Sponsors', GridField::create(
            'Sponsors',
            'Sponsors on this conference',
            $this->Sponsors(),
            GridFieldConfig_RecordEditor::create()
        ));

        $fields->addFieldToTab('Root.Location', GridField::create(
            'Locations',
            'Locations on this conference',
            $this->Locations(),
            GridFieldConfig_RecordEditor::create()
        ));

        $fields->addFieldToTab('Root.Room', GridField::create(
            'Rooms',
            'Rooms on this conference',
            $this->Rooms(),
            GridFieldConfig_RecordEditor::create()
        ));

        return $fields;
    }
}

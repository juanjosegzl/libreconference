<?php

namespace LibreOffice;

use PageController;
use SilverStripe\View\Requirements;
use SilverStripe\Control\HTTP;

class ConferencePageController extends PageController
{

    protected function init()
    {
        parent::init();
        Requirements::css('libreoffice/conference: client/dist/styles/bucharest2023.css');
        Requirements::javascript('libreoffice/conference: client/dist/js/yscountdown.js');
    }
}

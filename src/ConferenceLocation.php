<?php
namespace LibreOffice;

use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Assets\Image;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DataObject;
use SilverStripe\Versioned\Versioned;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;

class ConferenceLocation extends DataObject
{
    private static $table_name = 'LibreOfficeConferenceLocation';

    private static $db = [
        'Name' => 'Varchar',
        'About' => 'HTMLText',
        'Url' => 'Varchar',
    ];

    private static $has_one = [
        'Picture' => Image::class,
        'ConferencePage' => ConferencePage::class,
    ];

    private static $owns = [
        'Picture',
    ];

    private static $extensions = [
        Versioned::class,
    ];

    private static $summary_fields = [
        'Name',
        'Url',
    ];

    public function getCMSFields() {
        $fields = FieldList::create(
            TextField::create('Name'),
            HTMLEditorField::create('About'),
            TextField::create('Url'),
            $uploader = UploadField::create('Picture')
        );

        $uploader->setFolderName('conference-location-pictures');
        $uploader->getValidator()->setAllowedExtensions(['png', 'gif', 'jpg', 'jpeg']);

        return $fields;
    }
}

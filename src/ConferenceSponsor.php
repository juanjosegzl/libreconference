<?php
namespace LibreOffice;

use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Assets\Image;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DataObject;
use SilverStripe\Versioned\Versioned;

class ConferenceSponsor extends DataObject
{
    private static $table_name = 'LibreOfficeConferenceSponsor';

    private static $db = [
        'Name' => 'Varchar',
        'Url' => 'Varchar',
    ];

    private static $has_one = [
        'Logo' => Image::class,
        'ConferencePage' => ConferencePage::class,
    ];

    private static $owns = [
        'Logo',
    ];

    private static $extensions = [
        Versioned::class,
    ];

    private static $summary_fields = [
        'Name',
        'Url',
    ];

    public function getCMSFields() {
        $fields = FieldList::create(
            TextField::create('Name'),
            TextField::create('Url'),
            $uploader = UploadField::create('Logo')
        );

        $uploader->setFolderName('conference-sponsor-logos');
        $uploader->getValidator()->setAllowedExtensions(['png', 'gif', 'jpg', 'jpeg']);

        return $fields;
    }
}
